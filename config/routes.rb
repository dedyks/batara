
Rails.application.routes.draw do
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html

  namespace 'api' do
  	namespace 'v1' do
  		resources :pasiens
  		resources :dokters
  		resources :puskesmas
  		resources :rekam_medis
  	end
  end

end
