10.times do 
	Pasien.create({
			id_ktp: Faker::Number.number(15),
			nama_pasien: Faker::Name.name, 
			alamat_pasien: Faker::Address.street_name,
			jenis_kelamin: Faker::Boolean.boolean,
			tempat_lahir: Faker::Address.city,
			tanggal_lahir: Faker::Date.backward
		})
end

10.times do 
	Dokter.create({
		id_dokter: Faker::Number.number(5),
		status: Faker::Boolean.boolean,
		nama_dokter: Faker::Name.name,
		jenis_kelamin: Faker::Boolean.boolean,
		nama_puskesmas: Faker::Address.country,
		nama_poli: Faker::Address.city,
		jadwal_dokter: Faker::Date.backward

		})
end

10.times do 
	Puskesma.create({
			id_puskesmas: Faker::Number.number(5),
			nama_puskesmas: Faker::Address.city,
			alamat_puskesmas: Faker::Address.street_name,
			kontak: Faker::Internet.email,
			penanggung_jawab: Faker::Name.name
		})
end

10.times do 
	RekamMedi.create({
			nik: Faker::Number.number(10),
			nama_pasien: Faker::Name.name,
			nama_poli: Faker::Science.element,
			nama_dokter: Faker::Name.name,
			obat: Faker::Lorem.words,
			diagnosa: Faker::Lorem.sentence
		})	
end