class Api::V1::RekamMedisController < ApplicationController

	# GET /rekam_medis
	def index
		@rekam_medis = RekamMedi.all
		render json: @rekam_medis
	end

	#GET /rekam_medis/n
	def show
		render json: @rekam_medis
	end

	# POST /rekam_medis
	def create
		@rekam_medi = RekamMedi.new(rekam_medi_params)
		if @rekam_medi.save
			render json: @rekam_medi, status: :created, location: @rekam_medi
		else
			render json: @rekam_medi.error, status: :unprocessable_entity
		end
	end

	# PATCH/PUT rekam_medis/n
	def update
		@rekam_medi = RekamMedi.find(params[:id])
		if @rekam_medi.update(rekam_medi_params)
			render json: @rekam_medi
		else
			render json: @rekam_medi.error, status: :unprocessable_entity
		end
	end


	# DELETE /rekam_medi
	def destroy
		@rekam_medi = RekamMedi.find(params[:id])
		@puskesma.destroy
	end


	private 

	def rekam_medi_params 
		params.require(:rekam_medi).permit(:nik, :nama_pasien, :nama_poli, :nama_dokter, :obat, :diagnosa)
	end

end
