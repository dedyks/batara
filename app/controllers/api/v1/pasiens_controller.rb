class Api::V1::PasiensController < ApplicationController

	# GET /pasiens
	def index
		@pasiens = Pasien.all
		render json: @pasiens
	end

	#GET /pasiens/n
	def show
		@pasien = Pasien.find(params[:id])
		render json: @pasien
	end

	# POST /pasiens
	def create
		@pasien = Pasien.new(pasien_params)
		if @pasien.save
			render json: @pasien, status: :created, location: @pasien
		else
			render json: @pasien.error, status: :unprocessable_entity
		end
	end

	# PATCH/PUT pasiens/n
	def update
		@pasien = Pasien.find(params[:id])
		if @pasien.update(pasien_params)
			render json: @pasien
		else
			render json: @pasien.error, status: :unprocessable_entity
		end
	end


	# DELETE /pasiens
	def destroy
		@pasien = Pasien.find(params[:id])
		@pasien.destroy
	end


	private 

	def pasien_params 
		params.require(:pasien).permit(:id_ktp, :nama_pasien, :alamat_pasien, :jenis_kelamin, :tempat_lahir, :tanggal_lahir)
	end

end
